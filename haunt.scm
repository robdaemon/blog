(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt reader commonmark)
             (haunt site))

(site #:title "Rob Roland's Tech and Retro Notes"
      #:domain "robdaemon.codeberg.page"
      #:default-metadata
      '((author . "Robert Roland")
        (email  . "rob@retronauts.org"))
      #:build-directory "../pages"
      #:readers (list commonmark-reader)
      #:builders (list (blog)
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (static-directory "images")))
