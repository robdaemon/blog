title: Kate editor, Ada and the Ada Language Server
date: 2022-06-22 20:00
tags: ada kate
summary: Setting up kate and Ada
---

You can use the Ada Language Server with the KDE Kate editor
as opposed to sticking to VS Code, if you prefer Kate. It requires
a little setup, but I'll explain that now.

Start with building the Ada Language Server. I did this using
[Alire](https://alire.ada.dev/) with a [dedicated Alire index](https://github.com/reznikmm/als-alire-index).

After the build is complete, I copied the artifact to my `$HOME/bin` folder.

Next up, within Kate, set up the language server. Inside Kate, go to **Settings**,
**Configure Kate**, and **LSP Client**. Select the **User Server Settings** tab on the top of that
dialog, and insert the following:

```
{
    "servers": {
        "ada": {
            "command": ["ada_language_server"],
            "root": "",
            "url": "https://github.com/AdaCore/ada_language_server",
            "highlightingModeRegex": "^Ada$"
        }
    }
}
```

![Kate LSP User Server Settings](/images/kate-user-server-settings.png)

This assumes the `ada_language_server` binary is in your `PATH`. This will make the Ada Language
Server available for all instances of Kate.

Next up, within your project, you'll want to specify some additional settings via your `.kateproject`
file to select the proper gprbuild project file.

Here's a `.kateproject` file from one of my projects:

```
{
    "name": "myproject",
    "lspclient": {
        "servers": {
            "ada": {
                "settings": {
                    "ada": {
                        "projectFile": "myproject.gpr",
                        "enableIndexing": true
                    }
                }
            }
        }
    }
}
```

The nested groups will seem somewhat redundant, but this is the proper syntax between the language
server itself and Kate.
